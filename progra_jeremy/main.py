import random
import cv2

class Nodo:
    def __init__(self,clave, color, forma):
        self.id = clave
        self.color = color
        self.forma = forma
        self.conectadoA = {}

    def agregarVecino(self,vecino,peso):
        self.conectadoA[vecino] = peso

    def __str__(self):
        return str(self.id) + ' conectadoA: ' + str([x.id for x in self.conectadoA])

    def obtener_conexiones(self):
        return self.conectadoA.keys()

    def get_id(self):
        return self.id


    def obtener_ponderacion(self,vecino):
        
        for x in self.obtener_conexiones():
            if x.id == vecino:
                return self.conectadoA.get(x) # El get retorna el valor de la llave que se le pasa
                

        return None

    def __iter__(self):
        return iter(self.conectadoA.values())

class Grafo:

    def __init__(self):
        self.lista_nodos = {}
        self.cantidad_nodos = 0

    def agregar_nodo(self,clave, color, forma):

        self.cantidad_nodos = self.cantidad_nodos + 1
        nuevo_nodo = Nodo(clave, color, forma)
        self.lista_nodos[clave] = nuevo_nodo
        return nuevo_nodo

    def obtener_nodo(self,nodo):
        if nodo in self.lista_nodos:
            return self.lista_nodos[nodo]
        else:
            return None

    def __contains__(self,n):
        return n in self.lista_nodos

    def agregar_arista(self,nodo1,nodo2):
        peso = azar()
        
        if nodo1 not in self.lista_nodos:
            nuevo_nodo = self.agregar_nodo(nodo1, nodo1.color, nodo1.forma)

        if nodo2 not in self.lista_nodos:
            nuevo_nodo = self.agregar_nodo(nodo2, nodo2.color, nodo2.forma)

        self.lista_nodos[nodo1].agregarVecino(self.lista_nodos[nodo2], peso)

    def obtener_nodos(self):
        return self.lista_nodos.keys()

    def __iter__(self):
        return iter(self.lista_nodos.values())

def azar():
    valor = random.randint(1,99)
    return valor

def main():
    grafito = Grafo()

    grafito.agregar_nodo(1, "rojo", "cuadrado")
    grafito.agregar_nodo(2, "azul", "triángulo")
    grafito.agregar_nodo(3, "verde", "círculo")
    grafito.agregar_nodo(54, "café", "rectángulo")
    grafito.agregar_arista(1, 2)
    grafito.agregar_arista(1, 3)
    grafito.agregar_arista(1, 54)
    
    nodo = grafito.obtener_nodo(1)
    
    print("Conexiones del primer nodo")
    print([x.color for x in nodo.obtener_conexiones()])
    print([x.id for x in nodo.obtener_conexiones()])
    print([x.forma for x in nodo.obtener_conexiones()])
    print()
    
    print("Nodos contenidos en el grafo")
    for x in list(grafito.lista_nodos.values()):
        print("ID:", x.id)
        print("Forma:", x.forma)
        print("Color:", x.color)
        print()

    print()
    print(list(grafito.lista_nodos.keys()))

    nodo = grafito.obtener_nodo(1)
    print("Probando pesos existentes del 1")
    print(list(nodo.conectadoA.values()))
    print("\nProbando llaves del diccionario del nodo 1") # Van a salir objetos como tal
    print(list(nodo.conectadoA.keys()))
    print("\nObteniendo la ponderación del nodo adyacente 54")
    print(nodo.obtener_ponderacion(54))

    return ""
print(main())