"""
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
"""

#bibliotecas importadas
import argparse
import imutils
import cv2
import numpy as np

"""
Esta clase se encargará de recibir un contorno y devolver un nombre para esa figura
"""
class ShapeDetector:
    def __iniciar__(self):
        pass

#se comparan basicamente por numero de vertices
    def detectar(self, c):
        forma = "desconocida"
        peri = cv2.arcLength(c,True)
        approx = cv2.approxPolyDP(c,0.04 * peri, True)

        if len(approx) == 3:
            forma = "triangulo"
        #para el cuadrado y rectangulo se utiliza aspectratio para diferenciar los lados
        elif len(approx) == 4:
            (x,y,w,h)=cv2.boundingRect(approx)
            ar = w/float(h)
            if(ar >= 0.95 and ar <= 1.05): 
                forma = "cuadrado"
            else:
                forma = "rectangulo"
        elif len(approx) == 5:
            forma = "pentagono"
        else:
            forma = "circulo"
        #se retorna un string con el nombre de la figura
        return forma

#se asigna la imagen para ser leida
img = cv2.imread("figuris.jpeg")

#estos son rangos de color BGR a la izquierda van los oscuros y a la derecha los claros
rangos = [
	([0, 0, 100],[137, 137, 255]), #rojo
    ([85, 0, 0],[255, 150, 96]), #azul
    ([0, 180, 180],[157, 255, 255]), #amarillo
    ([0, 100, 0],[131, 255, 153]), #verde
    ([179, 0, 179],[255, 175, 255]) #morado
]

#Lista designada para guardar strings
lista_formas = []
#contador utilizado para saber la cantidad de veces que se ha realizado el ciclo
contador = 0

"""
Se realizan dos ciclos, uno para repasar toda la imagen diferenciando colores y 
otro para identificar figuras, primero por medio de las mascaras y los rangos establecidos se 
separan los rojos,amarillos y así sucesivamente. Se modifica la imagen para que quede en blanco y negro
Se buscan sus contornos y con base a estos se sacan las figuras
"""
for(lower,upper) in rangos:
    contador = contador + 1

    #esto es para los colores solo aisla los que son de un mismo color
    #basado en el rango
    lower = np.array(lower, dtype = "uint8")
    upper = np.array(upper, dtype = "uint8")

    mask = cv2.inRange(img,lower,upper)
    output = cv2.bitwise_and(img,img, mask = mask)

    n_tamanno = imutils.resize(output, width=300)
    ratio = img.shape[0] / float(n_tamanno.shape[0])

    #hacemos la imagen en escala de grises y luego a blanco y negro
    imgGris = cv2.cvtColor(n_tamanno,cv2.COLOR_BGR2GRAY)
    _, limite = cv2.threshold(imgGris, 20, 255, cv2.THRESH_BINARY)

    cnts = cv2.findContours(limite.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnts = imutils.grab_contours(cnts)
    sd = ShapeDetector()

    #por medio del contador establecemos en cual rango estamos
    color = "desconocido"
    if contador == 1:
        color = "rojo"
    elif contador == 2:
        color = "azul"
    elif contador == 3:
        color = "amarillo"
    elif contador == 4:
        color = "verde"
    else:
        color = "morado"

    #en este otro ciclo se sacan los contornos de cada forma detectada
    #y se envian a detectar para determinar su figura
    for c in cnts:
        M = cv2.moments(c)
        #validación para eliminar y pixeles y basura de la pantalla
        if M["m00"] < 50:
            continue
        else:
            cX = int((M["m10"] / M["m00"]) * ratio)
            cY = int((M["m01"] / M["m00"]) * ratio)
            forma = sd.detectar(c)
            #guarda la información en la lista
            lista_formas.append(forma)
            lista_formas.append(color)

        cv2.imshow("Imagen siendo escaneada",img)
        print(lista_formas)
        cv2.waitKey(0)


