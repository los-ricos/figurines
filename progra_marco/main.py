import random
import algoritmo_prim as prim
import algoritmo_floyd_warshall as floyd
import shapes as ShapeDetector

"""
Clase para cada nodo del grafo
"""
class Nodo:

    """
    Constructor de la clase Nodo
    """
    def __init__(self,clave, color, forma):
        self.id = clave
        self.color = color
        self.forma = forma
        self.conectadoA = {}

    """
    Función: Agregar un nodo adyacente
    Entradas: Ninguna
    Salidas: Ninguna
    """
    def agregar_adyacente(self,vecino,peso):
        self.conectadoA[vecino] = peso
    """
    Función: Obtener las llaves del los nodos adyacentes del nodo
    Entradas: Ninguna
    Salidas: Retorna las llaves de los nodos adyacentes
    """
    def obtener_conexiones(self):
        return self.conectadoA.keys()

    """
    Función: Obtener los pesos de todos los nodos adyacentes del nodo
    Entradas: Ninguna
    Salidas: Retorna los pesos de los nodos adyacentes
    """
    def obtener_pesos(self):
        return self.conectadoA.values()
    """
    Función: Obtener el id de un nodo
    Entradas: Ninguna
    Salidas: Retorna el id del nodo
    """
    def get_id(self):
        return self.id

    """
    Función: Obtener un peso específico
    Entradas: El nodo adyacente a ser obtenido el peso
    Salidas: Retorna el peso correspondiente
    """
    def obtener_ponderacion(self,vecino):
        
        for x in self.obtener_conexiones():
            if x.id == vecino:
                return self.conectadoA.get(x) # El get retorna el valor de la llave que se le pasa

        return None
    """
    Función: Sobreescribe el iterador
    Entradas: Ninguna
    Salidas: Retorna un iterador con los valores del nodo actual
    """
    def __iter__(self):
        return iter(self.conectadoA.values())

"""
Clase grafo que contiene una lista con los nodos creados
"""
class Grafo:

    """
    Constructor de la clase Grafo
    """
    def __init__(self):
        self.lista_nodos = {}
        self.cantidad_nodos = 0

    """
    Función: Agregar un nodo al grafo
    Entradas: Recibe como parámetros el ID, color y forma de la figura
    Salidas: Retorna el nuevo nodo creado
    """
    def agregar_nodo(self,clave, color, forma):

        self.cantidad_nodos = self.cantidad_nodos + 1
        nuevo_nodo = Nodo(clave, color, forma)
        self.lista_nodos[clave] = nuevo_nodo
        return nuevo_nodo

    """
    Función: Obtener un nodo de la lista de nodos
    Entradas: Un nodo a obtener
    Salidas: Retorna el contenido del nodo si lo encuentra, si no es así, retorna None
    """
    def obtener_nodo(self,nodo):
        if nodo in self.lista_nodos:
            return self.lista_nodos[nodo]
        else:
            return None

    """
    Función: Agregar un nodo adyacente a un nodo
    Entradas: Recibe como parámetro 2 nodos a conectar
    Salidas: Ninguna
    """
    def agregar_arista(self,nodo_origen,nodo_destino):
        # Crea el peso random que tendrán los 2 nodos
        peso = azar()

        # Verifica que los 2 nodos estén en el grafo
        if nodo_origen not in self.lista_nodos:
            nuevo_nodo = self.agregar_nodo(nodo_origen, nodo_origen.color, nodo_origen.forma)

        if nodo_destino not in self.lista_nodos:
            nuevo_nodo = self.agregar_nodo(nodo_destino, nodo_destino.color, nodo_destino.forma)

        # Verifica que los nodos a conectar sean diferentes
        if self.lista_nodos[nodo_origen].id != self.lista_nodos[nodo_destino].id:

            # Si son diferentes, comprueba si tienen alguna similitud entre sus colores o formas
            if self.lista_nodos[nodo_origen].color == self.lista_nodos[nodo_destino].color or self.lista_nodos[nodo_origen].forma == self.lista_nodos[nodo_destino].forma:
                
                # Sí es así, agrega las conexiones a los dos
                self.lista_nodos[nodo_origen].agregar_adyacente(self.lista_nodos[nodo_destino], peso)

                self.lista_nodos[nodo_destino].agregar_adyacente(self.lista_nodos[nodo_origen], peso)

    """
    Función: Obtener los nodos del grafo
    Entradas: Ninguna
    Salidas: Retorna las llaves de los nodos del grafo
    """
    def obtener_nodos(self):
        return self.lista_nodos.keys()

    """
    Función: Sobreescribe el iterador
    Entradas: Ninguna
    Salidas: Retorna un iterador con los valores del nodo actual
    """
    def __iter__(self):
        return iter(self.lista_nodos.values())

"""
Función para conseguir un número random
Entradas: Ninguna
Salidas: Número random entre 1 y 99
Restricciones: Ninguna
"""
def azar():
    valor = random.randint(1,99)
    return valor

"""
Función para imprimir el grafo actual
Entradas: Recibe como parámetros un grafo
Salidas: Ninguna
Restricciones: Ninguna
"""
def imprimir_grafo(grafito):

    for x in list(grafito.lista_nodos.values()):
        print("ID:", x.id)
        print("Forma:", x.forma)
        print("Color:", x.color)
        for y in x.conectadoA:
            print("Conectado a:", y.forma, ", ID:", y.id, " y peso:", x.conectadoA[y])
        print()

"""
Función encargada de leer la imagen y cargar las figuras en la estructura de grafo
Entradas: Recibe como parámetro un grafo
Salidas: Ninguna
Restricciones: Ninguna
"""
def leer_figuras(grafito):

    figuras = ShapeDetector.shapes()
    i = 0

    for forma,color in figuras:
        grafito.agregar_nodo(i, color, forma)
        i += 1

    for i in range(grafito.cantidad_nodos):

        for j in range(grafito.cantidad_nodos):

            grafito.agregar_arista(i,j)

    return grafito

"""
Función encargada del flujo principal del programa
Entradas: Ninguna
Salidas: Ninguna
Restricciones: Ninguna
"""
def main():

    grafito = Grafo()
    estado = True

    while(estado):
        print("+------------------------------+")
        print("+        Menú Principal        +")
        print("+  0. Salir del programa       +")
        print("+  1. Leer imagen              +")
        print("+  2. Imprimir grafo           +")
        print("+  3. Algoritmo Prim           +")
        print("+  4. Algoritmo Floyd-Warshall +")
        print("+------------------------------+")
        opcion = input("Elija su opción: ")

        opcion = int(opcion)

        if opcion == 0:
            print("Hasta luego amiguit@...")
            break
        
        elif opcion == 1:
            grafito = leer_figuras(grafito)

        elif opcion == 2:
            imprimir_grafo(grafito)

        elif opcion == 3:
            prim.show_prim(grafito)

        elif opcion == 4:
            floyd.floyd_warshall(grafito)

        else:
            print("Ingrese un número entre 0 y 4")

    return ""

print(main())

