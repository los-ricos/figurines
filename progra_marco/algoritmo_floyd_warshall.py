"""
Tecnologico de Costa Rica
Estructuras de Datos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
"""

"""
Función para imprimir la matriz
Entradas: Una matriz y una lista de nodos totales contenidos en el grafo
Salidas: Ninguna
Restricciones: Ninguna
"""
def imprimir(matriz, nodos):

    # Primera fila de números
    for i in range(len(nodos)):
        if i == 0:
            print("  ", nodos[i], end="    ")

        else:
            print(nodos[i], end="    ")
    
    print("")
    
    # Ciclos para imprimir el contenido de la matriz
    for i in range(len(matriz)):

        for j in range(len(matriz[i])):

            if j == 0:
                print(str(nodos[i])+" "+str(matriz[i][j]), end="  ")

            else:
                print(" "+str(matriz[i][j]), end="  ")

        print("")

    return ""

"""
Función para crear la matriz a partir de los elementos contenidos en el grafo
Entradas: Entra un grafo como parámetro
Salidas: Retorna la matriz creada
Restricciones: Ninguna
"""
def crear_matriz(grafito):
    matriz = []
    
    for i in range(grafito.cantidad_nodos):
        matriz.append([])
        nodo = grafito.obtener_nodo(i)
        nodos_conectados = list(nodo.obtener_conexiones())
        pesos = list(nodo.obtener_pesos())
        x = 0

        for j in range(grafito.cantidad_nodos):
            
            if x < len(nodos_conectados):
                if j == nodos_conectados[x].id:
                    if pesos[x] < 10:
                        matriz[i].append(" "+str(pesos[x]))
                        x += 1

                    else:
                        matriz[i].append(str(pesos[x]))
                        x += 1
                else:
                    if j == i:
                        matriz[i].append(" 0")
                    else:
                        matriz[i].append(" ∞")
            
            else:
                if j == i:
                    matriz[i].append(" 0")
                
                else:
                    matriz[i].append(" ∞")

    return matriz

"""
Función encargada de realizar el algoritmo Floyd-Warshall
Entradas: Una matriz generada, la matriz transpuesta de esa matriz y la actual línea bloqueada 
Salidas: Retorna la matriz modificada
Restricciones: Ninguna
"""
def operar_matriz(matriz, matriz_transpuesta, linea):
    # Se obtienen la fila y columna bloqueadas de la matriz
    fila = matriz[linea]
    columna = matriz_transpuesta[linea]

    # Empiezan los ciclos para recorrer la matriz
    for i in range(len(matriz)):

        for j in range(len(matriz)):

            # Es la diagonal, que se mantiene siempre en un valor de "0"
            if j == i:
                continue
            
            # Significa que está en una fila bloqueada, entonces mantiene el valor
            elif i == linea:
                continue

            # Significa que está en una columna bloqueada, entonces mantiene el valor
            elif j == linea:
                continue

            else:
                # Si el valor actual de la matriz y los valores de las líneas bloqueadas son
                # diferentes de infinito, procederá a operar y realizar la comparación
                if matriz[i][j] != " ∞":
                    if fila[j] != " ∞" and columna[i] != " ∞":
                        suma = int(fila[j])+int(columna[i])

                        if suma < int(matriz[i][j]):
                            matriz[i][j] = str(suma)
                        
                        else:
                            matriz[i][j] = matriz[i][j]

                # Si el valor actual de la matriz es infinito, pregunta por los valores
                # de las líneas bloqueadas y si son diferentes de infinito, coloca la suma de ellas
                # en el campo actual de la matriz
                else:
                    if fila[j] != " ∞" and columna[i] != " ∞":
                        suma = int(fila[j])+int(columna[i])
                        matriz[i][j] = str(suma)

    return matriz

"""
Función para calcular la transpuesta de una matriz.
Entradas: Una matriz
Salidas: Retorna la matriz transpuesta
Restricciones: Ninguna
"""
def transpuesta(matriz):

    i = 0
    j = 0
    n = len(matriz)
    m = len(matriz[0])
    nueva_fila = []
    nueva_matriz = []

    while j < m:
        while i < n:
            nueva_fila = nueva_fila+[matriz[i][j]]
            i += 1
        i = 0
        j += 1
        nueva_matriz = nueva_matriz+[nueva_fila]
        nueva_fila = []

    return nueva_matriz

"""
Función encargada del flujo del algoritmo
Entradas: Recibe un grafo como parámetro
Salidas: Ninguna
Restricciones: Ninguna
"""
def floyd_warshall(grafito):
    
    matriz = crear_matriz(grafito)

    print()
    print("Matriz inicial")
    imprimir(matriz, list(grafito.obtener_nodos()))
    print()

    # Comienza a realizar las iteraciones del algoritmo
    x = 0
    while x < len(matriz):
        print("Iteración", x)
        matriz_transpuesta = transpuesta(matriz)
        matriz = operar_matriz(matriz,matriz_transpuesta,x)
        imprimir(matriz, list(grafito.obtener_nodos()))
        print()
        x += 1

    return ""

if __name__ == "__main__":
    pass