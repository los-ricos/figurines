"""
Tecnologico de Costa Rica
Estructuras de Datos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
"""

"""
Función para realizar el algoritmo de prim
Entradas: Recibe un grafo como parámetro
Salidas: Ninguna
Restricciones: Ninguna
"""
def show_prim(grafo):

    lista_visitados = []
    lista_ordenada = []

    # Pide un nodo al usuario
    origen = input("\nIngrese el nodo origen: ")

    # Lo agrega a los visitados
    lista_visitados.append(origen)
    

    # Agrega los nodos adyacentes del nodo elegido a la lista
    nodo_elegido = grafo.obtener_nodo(int(origen))

    # Almaceno objetos y la distancia que hay entre ellos en la lista
    # para poder acceder a sus propias cosas luego
    for x in nodo_elegido.conectadoA:
        lista_ordenada.append((nodo_elegido, x, int(nodo_elegido.conectadoA[x])))


    # Ordena esa lista
    posicion = 0
    peso = 0
    lista_auxiliar = []

    for i in range(len(lista_ordenada)):
        lista_auxiliar = lista_ordenada[i]
        peso = lista_ordenada[i][2]
        posicion = i

        while posicion > 0 and lista_ordenada[posicion-1][2] > peso:
            lista_ordenada[posicion] = lista_ordenada[posicion-1]
            posicion = posicion-1

        lista_ordenada[posicion] = lista_auxiliar

    # Empieza a realizar el agloritmo prim con ayuda de un ciclo
    while lista_ordenada:
      
      nodo = lista_ordenada.pop(0)
      nodo_destino = nodo[1].id

      if nodo_destino not in lista_visitados:
        lista_visitados.append(nodo_destino)

        nodo_elegido = grafo.obtener_nodo(nodo_destino)

        # Busca los adyacentes del nodo x
        for x in nodo_elegido.conectadoA:
          if x.id not in lista_visitados:
            lista_ordenada.append((nodo_elegido,x,int(nodo_elegido.conectadoA[x])))

        # Se ordena la lista nuevamente
        lista_ordenada = [(c,a,b) for a,b,c in lista_ordenada]
        lista_ordenada.sort()
        lista_ordenada = [(a,b,c) for c,a,b in lista_ordenada]

        origen = nodo[0].forma
        destino = nodo[1].forma
        peso = nodo[2]
        print()
        print(origen, destino, peso)

if __name__ == "__main__":
    pass